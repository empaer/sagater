package empaer.mt.gov.br.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import empaer.mt.gov.br.model.Municipio;
import empaer.mt.gov.br.model.Regiao;
import empaer.mt.gov.br.util.HibernateUtil;

public class MunicipioDAO {
	private Session session;
	public Municipio findMunicipioByID(Integer idMunicipio){
		Municipio municipio = null;
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select m ");
			sb.append(" from Municipio as m ");
			sb.append(" where m.idMunicipio =:idMunicipio");
			
			
			Query query = session.createQuery(sb.toString());
			
			query.setParameter("idMunicipio", idMunicipio); 
			
			
			municipio = (Municipio) query.uniqueResult();
			
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return municipio;
	}
	
	public List<Municipio> listMunicipioBySigla(String siglaEstado){
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select m ");
			sb.append(" from Municipio as m ");
			sb.append(" where m.siglaEstado =:siglaEstado");
			
			Query query = session.createQuery(sb.toString());
			
			query.setParameter("siglaEstado", siglaEstado);
			
			listaMunicipio = query.list();
			
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return listaMunicipio;
		
	}
	
	public List<Regiao> listRegiao(){
		List<Regiao> listaRegiao = new ArrayList<Regiao>();
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			
			sb.append(" from Regiao ");
			
			
			Query query = session.createQuery(sb.toString());
			
			
			
			listaRegiao = query.list();
			
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return listaRegiao;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Municipio> listMunicipioByRegiao(Integer idRegiao){
		List<Municipio> listaMunicipio = new ArrayList<Municipio>();
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select m ");
			sb.append(" from Municipio as m ");
			sb.append(" where m.regiao.idRegiao =:idRegiao");
			
			Query query = session.createQuery(sb.toString());
			
			query.setParameter("idRegiao", idRegiao);
			
			listaMunicipio = query.list();
			
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
		return listaMunicipio;
		
	}
}
