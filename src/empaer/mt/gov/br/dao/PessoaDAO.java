package empaer.mt.gov.br.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;



import empaer.mt.gov.br.model.Escritorio;
import empaer.mt.gov.br.model.Pessoa;
import empaer.mt.gov.br.util.HibernateUtil;

public class PessoaDAO {
	private Session session;
	public void salvarAtualizar(Pessoa  pessoa){
		session = HibernateUtil.getSession();
		Transaction transaction = null;
		 try{
			
			 	session = HibernateUtil.getSession();
				transaction  = session.beginTransaction();
				session.saveOrUpdate(pessoa);
				transaction.commit();
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 if(session.isOpen()){
					session.close();
				}
		 }
	}
	
	public void excluir(Pessoa  pessoa){
		session = HibernateUtil.getSession();
		Transaction transaction = null;
		 
			
			 	session = HibernateUtil.getSession();
				transaction  = session.beginTransaction();
				session.delete(pessoa);
				transaction.commit();
		
			
		
			 if(session.isOpen()){
					session.close();
				}
		 
	}
	
	@SuppressWarnings("unchecked")
	public List<Pessoa> pesquisaGenerica(String campo,String valor){
		
		 	session = HibernateUtil.getSession();
		    List<Pessoa> lista = new ArrayList<Pessoa>();
		    try {
		    	session.beginTransaction();
		    	
		    	
		    		lista = (ArrayList<Pessoa>)session.createQuery(" from Pessoa as f where f." + campo + " like :valor").setParameter("valor", "%" + valor.toUpperCase() + "%").list();
		   	     		   
			    }catch(Exception e){
			    	e.printStackTrace();
			    	
			    }finally {
			    	session.close();
			     // entityManager.close();
			    }
			    return lista;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Pessoa> listPessoa(){
		session = HibernateUtil.getSession();
	    List<Pessoa> lista = new ArrayList<Pessoa>();
	    try {
	    		session.beginTransaction();
	    	   	lista = (ArrayList<Pessoa>)session.createQuery(" from Pessoa as f ").list();
	   	     		   
		    }catch(Exception e){
		    	e.printStackTrace();
		    	
		    }finally {
		    	session.close();
		     
		    }
		    return lista;
	}
	
	public Pessoa pesquisarPessoaPorCPF(String cpf){
		Pessoa pessoa = null;
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select p ");
			sb.append(" from Pessoa as p ");
			sb.append(" where p.cpf =:cpf");
			
			
			Query query = session.createQuery(sb.toString());
			
			query.setParameter("cpf", cpf);
			
			
			pessoa = (Pessoa) query.uniqueResult();
			
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return pessoa;
	}

}
