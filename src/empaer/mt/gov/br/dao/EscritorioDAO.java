package empaer.mt.gov.br.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import empaer.mt.gov.br.model.Escritorio;
import empaer.mt.gov.br.util.HibernateUtil;

public class EscritorioDAO {
	private Session session;
	
	public Escritorio findEscritorioByID(Integer idEscritorio){
		Escritorio escritorio = null;
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select e ");
			sb.append(" from Escritorio as e ");
			sb.append(" where e.idEscritorio =:idEscritorio");
			
			
			Query query = session.createQuery(sb.toString());
			
			query.setParameter("idEscritorio", idEscritorio);
			
			
			escritorio = (Escritorio) query.uniqueResult();
			
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		return escritorio;
	}
	
	public void salvar(Escritorio escritorio){
		Transaction transaction = null;
		try{
			session = HibernateUtil.getSession();
			transaction  = session.beginTransaction();
			session.save(escritorio);
			transaction.commit();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(session.isOpen()){
				session.close();
			}
		}
		
	}

}
