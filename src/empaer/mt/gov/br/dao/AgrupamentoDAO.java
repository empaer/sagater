package empaer.mt.gov.br.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import empaer.mt.gov.br.model.Agrupamento;
import empaer.mt.gov.br.model.Pessoa;
import empaer.mt.gov.br.util.HibernateUtil;
import empaer.mt.gov.br.util.Util;

public class AgrupamentoDAO {
	private Session session;
	
	public void salvarAtualizar(Agrupamento agrupamento){
		session = HibernateUtil.getSession();
		Transaction transaction = null;
		 try{
			
			 	session = HibernateUtil.getSession();
				transaction  = session.beginTransaction();
				session.saveOrUpdate(agrupamento);
				transaction.commit();
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 if(session.isOpen()){
					session.close();
				}
		 }
	}
	
	@SuppressWarnings("unchecked")
	public List<Agrupamento> pesquisaGenerica(String campo,String valor){ 
		
		 	session = HibernateUtil.getSession();
		    List<Agrupamento> lista = new ArrayList<Agrupamento>();
		    try {
		    	session.beginTransaction();
		    	
		    	StringBuilder sb = new StringBuilder();
			    	sb.append(" select f ");
					sb.append(" from Agrupamento as f ");
					sb.append(" where f.tipo='C' ");
				
				if(campo.equals("municipio")){
					sb.append(" and f.municipio.nomeMunicipio like '%" + valor.toUpperCase() + "%'" );	
				}else if(campo.equals("nome")){
					sb.append(" and f.nome like '%" + valor.toUpperCase() + "%'" );
				}else if(campo.equals("caracteristica")){
					String caracte= Util.tipoComunidade(valor);
					sb.append(" and f.caracteristica like '%" + caracte.toUpperCase() + "%'" );
				}
				
				
				
		    	
		    	lista = (ArrayList<Agrupamento>)session.createQuery(sb.toString()).list();
		   	     		   
			    }catch(Exception e){
			    	e.printStackTrace();
			    	
			    }finally {
			    	session.close();
			     
			    }
			    return lista;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Agrupamento> pesquisaGenericaAssentamento(String campo,String valor){ 
		
		 	session = HibernateUtil.getSession();
		    List<Agrupamento> lista = new ArrayList<Agrupamento>();
		    try {
		    	session.beginTransaction();
		    	
		    	StringBuilder sb = new StringBuilder();
			    	sb.append(" select f ");
					sb.append(" from Agrupamento as f ");
					sb.append(" where f.tipo='A' ");
				
				if(campo.equals("municipio")){
					sb.append(" and f.municipio.nomeMunicipio like '%" + valor.toUpperCase() + "%'" );	
				}else if(campo.equals("nome")){
					sb.append(" and f.nome like '%" + valor.toUpperCase() + "%'" );
				}else if(campo.equals("caracteristica")){
					String caracte= Util.tipoComunidade(valor);
					sb.append(" and f.caracteristica like '%" + caracte.toUpperCase() + "%'" );
				}
				
				
				
		    	
		    	lista = (ArrayList<Agrupamento>)session.createQuery(sb.toString()).list();
		   	     		   
			    }catch(Exception e){
			    	e.printStackTrace();
			    	
			    }finally {
			    	session.close();
			     
			    }
			    return lista;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Agrupamento> listAgrupamento(String tipo){
		session = HibernateUtil.getSession();
	    List<Agrupamento> lista = new ArrayList<Agrupamento>();
	    try {
	    		session.beginTransaction();
	    		
	    		StringBuilder sb = new StringBuilder();
		    	sb.append(" select f ");
				sb.append(" from Agrupamento as f ");
				sb.append(" where f.tipo =:tipo ");
	    		
	    		
				Query query = session.createQuery(sb.toString());
	    	   	
	    	   	query.setParameter("tipo", tipo);
	    	   	
	    	   	lista = (ArrayList<Agrupamento>) query.list();
	   	     		   
		    }catch(Exception e){
		    	e.printStackTrace();
		    	
		    }finally {
		    	session.close();
		     
		    }
		    return lista;
	}
	
	public void excluir(Agrupamento  agrupamento){
			session = HibernateUtil.getSession();
			Transaction transaction = null;
		 
			
			 	session = HibernateUtil.getSession();
				transaction  = session.beginTransaction();
				session.delete(agrupamento);
				transaction.commit();
		
			
		
			 if(session.isOpen()){
					session.close();
				}
		 
	}
	
	
}
