package empaer.mt.gov.br.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import empaer.mt.gov.br.model.Escritorio;
import empaer.mt.gov.br.model.EscritorioUsuario;
import empaer.mt.gov.br.model.Usuario;
import empaer.mt.gov.br.util.HibernateUtil;

public class UsuarioDAO {
	private Session session;
	
	
	public List<Object[]> listUsuario(){
		List<Object[]> usuarios=null;
		 try {
			 session = HibernateUtil.getSession();
			 session.beginTransaction();
			 StringBuilder sb = new StringBuilder();
			 sb.append("SELECT ");
			 sb.append("USR_LOGIN, ");
			 sb.append("USR_NOME, ");
			 sb.append("USR_EMAIL ");
			 sb.append(" FROM FR_USUARIO" );
			 Query q = session.createSQLQuery(sb.toString());
			 usuarios = q.list();
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 session.close();
		 }
		 
		 return usuarios;
	}
	
	public Usuario usurioByLoginBySenha(String login,String senha){
		Usuario usuario = null;
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select u ");
			sb.append(" from Usuario as u ");
			sb.append(" where u.login =:login");
			sb.append(" and u.senha =:senha");
			
			Query query = session.createQuery(sb.toString());
			
			query.setString("login", login);
			query.setString("senha", senha);
			
			usuario = (Usuario) query.uniqueResult();
		
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			session.close();
		}
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public List<EscritorioUsuario> listEscritorioUsuarioByIdUsuario(Integer idUsuario){
		List<EscritorioUsuario> listaEscritorioUsuario = new ArrayList<EscritorioUsuario>();
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select u ");
			sb.append(" from EscritorioUsuario as u ");
			sb.append(" where u.usuario =:idUsuario");
			
			
			Query query = session.createQuery(sb.toString());
			
			query.setInteger("idUsuario", idUsuario);
		
			
			listaEscritorioUsuario = query.list();
		
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			session.close();
		}
		return listaEscritorioUsuario;
	}
	
	public Usuario usuarioById(Integer idUsuario){
			Usuario usuario = null;
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			
			StringBuilder sb = new StringBuilder();
			sb.append(" select u from Usuario u");
			sb.append(" where u.idUsuario =:idUsuario");
			
			Query query = session.createQuery(sb.toString());
			
			query.setInteger("idUsuario", idUsuario);
			
			usuario = (Usuario) query.uniqueResult();
			
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 session.close();
		 }
		
		return usuario;
		
	}
	
	
	public List<Escritorio> listEscritorioByIdUsuario(Integer idUsuario){
		List<Escritorio> listaEscritorio = new ArrayList<Escritorio>();
		try{
			session = HibernateUtil.getSession();
			session.beginTransaction();
			StringBuilder sb = new StringBuilder();
			sb.append(" select u.escritorio ");
			sb.append(" from EscritorioUsuario as u ");
			sb.append(" where u.usuario =:idUsuario");
			
			
			Query query = session.createQuery(sb.toString());
			
			query.setInteger("idUsuario", idUsuario);
		
			
			listaEscritorio = query.list();
		
		}catch(Exception e){
			e.printStackTrace();
		
		}finally{
			session.close();
		}
		return listaEscritorio;
	}
}
