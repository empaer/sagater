package empaer.mt.gov.br.relatorio;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.hibernate.Query;
import org.hibernate.Session;

import empaer.mt.gov.br.model.AssentamentoAtendidoVO;
import empaer.mt.gov.br.model.MunicipioAtendidoVO;
import empaer.mt.gov.br.model.RelatorioAreaAtuacaoVO;
import empaer.mt.gov.br.util.HibernateUtil;

public class RelatorioAreaAtuacao {
	private Session session;
	public void gerarRelatorio(Integer idRegiao,Integer idMunicipio,String exercicio)throws JRException, IOException{
		
		
		List<AssentamentoAtendidoVO> lista = this.preparaDadosAssentamento(idRegiao,idMunicipio,exercicio);
		List<MunicipioAtendidoVO> listaMunicipiosVO = this.preparaDadosMunicipio(idRegiao,idMunicipio,exercicio);
		
		
		RelatorioAreaAtuacaoVO relAreaAtuacaoVO= new RelatorioAreaAtuacaoVO();
		relAreaAtuacaoVO.setListaAssentamentoAtendido(lista);
		relAreaAtuacaoVO.setListaMunicipioAtendido(listaMunicipiosVO);
		
		List<RelatorioAreaAtuacaoVO> listaRelatorioAreaAtuacaoVO =  new ArrayList<RelatorioAreaAtuacaoVO>();
		listaRelatorioAreaAtuacaoVO.add(relAreaAtuacaoVO);
		try{
			FacesContext context = FacesContext.getCurrentInstance();  
	        HttpServletResponse response = (HttpServletResponse) context  
	        .getExternalContext().getResponse();  
	  
	         
	        ServletOutputStream responseStream = response.getOutputStream();
	        ClassLoader classLoader = getClass().getClassLoader();    
	         
	        InputStream in = getClass().getResourceAsStream("empaer/mt/gov/br/relatorio/area_atuacao.jrxml"); 
	        InputStream ino = getClass().getResourceAsStream("area_atuacao.jrxml"); 
	        
	        
	        String arquivo1 = FacesContext.getCurrentInstance().getExternalContext().getRealPath("WEB-INF"+ File.separator + "imagens" + File.separator + "LOGO_EMPAER _2015.jpg");  
	        File nomearquivo = new File(arquivo1);     
	        
	       
	        
	        
	        // envia a resposta com o MIME Type  
	        response.setContentType("application/pdf");  
	  
	       
	        String nomeRel = "Relatorio �rea de Atua��o";
	        response.setHeader("Content-Disposition", "attachment; filename="+ nomeRel +".pdf");
	    
	        response.setContentType("application/pdf");
	        response.setHeader("Pragma", "no-cache");
	        
	        JasperReport pathReport = JasperCompileManager.compileReport(ino);
	        
	        Map parameters = new HashMap();     
	        parameters.put("LOGO",nomearquivo);

	      //relatorio gerado

	      JasperPrint preencher = JasperFillManager.fillReport(pathReport, parameters,new JRBeanCollectionDataSource(listaRelatorioAreaAtuacaoVO));
	    
	      
	      
	      JasperExportManager.exportReportToPdfStream(preencher,responseStream);
	      

	     
	      
		      responseStream.flush();
		
		      responseStream.close();
		
		      context.renderResponse();
		
		      context.responseComplete();
	       }catch(Exception e){
	    	   e.printStackTrace();
	    	   
	       }
		
	}
	
	//SQL para pegar n�mero de atendimento por assentamento
	@SuppressWarnings("unchecked")
	public  List<Object[]> sqlAssentamentoAtendido(Integer idRegiao,Integer idMunicipio,String exercicio){
		List<Object[]> listaAssentamentos=null;
		StringBuilder sb = new StringBuilder();
		 sb.append(" SELECT ");
		 sb.append(" AGRUPAMENTO.AGRU_NOME AS NOME, ");
		 sb.append(" COUNT(AGRUPAMENTO.AGRU_NOME)AS TOTAL, ");
		 sb.append(" CASE ben.ben_programa ");
		 sb.append(" WHEN 1 THEN 'MDA/ATER' ");
		 sb.append(" WHEN 2 THEN 'Chamada P�blica' ");
		 sb.append(" WHEN 3 THEN 'ATER Normal' ");
		 sb.append(" END as programa ");
		 sb.append(" FROM emptb_026_atendimento ATENDIMENTO ");
		 sb.append(" INNER JOIN emptb_010_agrupamento AGRUPAMENTO ON ATENDIMENTO.id_agrupamento = AGRUPAMENTO.id_agrupamento " );
		 sb.append(" INNER JOIN emptb_014_municipio municipio ON municipio.id_municipio = atendimento.id_municipio " );
		 sb.append(" INNER JOIN emptb_008_pessoa pessoa ON ATENDIMENTO.id_pessoa = pessoa.id_pessoa " );
		 sb.append(" INNER JOIN emptb_009_beneficiario ben ON pessoa.id_pessoa = ben.id_pessoa " );
		 sb.append(" WHERE 1=1 " );
		 if(idRegiao!=null && idRegiao!=0){
			 sb.append(" and municipio.id_regiao=" + idRegiao); 
		 }
		 if(idMunicipio!=null && idMunicipio!=0){
			 sb.append(" and municipio.id_municipio =" + idMunicipio);
		 }
		 if(exercicio!=null && !exercicio.equals("")){
			 sb.append(" and Extract('Year' From ate_data_visita) =" + exercicio ); 
		 }
		 
		 sb.append(" GROUP BY AGRUPAMENTO.AGRU_NOME,ben.ben_programa ");
		 sb.append(" ORDER BY AGRUPAMENTO.AGRU_NOME " );
		 try {
			 session = HibernateUtil.getSession();
			 session.beginTransaction();
			 Query q = session.createSQLQuery(sb.toString());
			 listaAssentamentos = q.list();
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 session.close();
		 }
		 
		 return listaAssentamentos;
	}
	
	@SuppressWarnings("unchecked")
	public  List<Object[]> sqlMunicipiosAtendidos(Integer idRegiao,Integer idMunicipio,String exercicio){
		List<Object[]> listaMunicipios=null;
		StringBuilder sb = new StringBuilder();
		 sb.append(" SELECT ");
		 sb.append(" MUNICIPIO.MUN_NOME, ");
		 sb.append(" COUNT(MUNICIPIO.MUN_NOME)AS TOTAL, ");
		 sb.append(" CASE ben.ben_programa  ");
		 sb.append(" WHEN 1 THEN 'MDA/ATER' ");
		 sb.append(" WHEN 2 THEN 'Chamada P�blica'  ");
		 sb.append(" WHEN 3 THEN 'ATER Normal' ");
		 sb.append(" END as programa ");
		 sb.append(" FROM emptb_026_atendimento ATENDIMENTO ");
		 sb.append(" INNER JOIN emptb_014_municipio MUNICIPIO ON ATENDIMENTO.ID_MUNICIPIO = MUNICIPIO.ID_MUNICIPIO " );
		 sb.append(" INNER JOIN emptb_008_pessoa pessoa ON ATENDIMENTO.id_pessoa = pessoa.id_pessoa ");
		 sb.append(" INNER JOIN emptb_009_beneficiario ben ON pessoa.id_pessoa = ben.id_pessoa ");
		 sb.append(" WHERE 1=1 " );
		 if(idRegiao!=null && idRegiao!=0){
			 sb.append(" and municipio.id_regiao=" + idRegiao); 
		 }
		 if(idMunicipio!=null && idMunicipio!=0){
			 sb.append(" and municipio.id_municipio =" + idMunicipio);
		 }
		 if(exercicio!=null && !exercicio.equals("")){
			 sb.append(" and Extract('Year' From ate_data_visita) =" + exercicio ); 
		 }
		 sb.append(" GROUP BY MUNICIPIO.MUN_NOME,ben.ben_programa ");
		 sb.append(" order by MUNICIPIO.MUN_NOME, ben.ben_programa ");
		 try {
			 session = HibernateUtil.getSession();
			 session.beginTransaction();
			 Query q = session.createSQLQuery(sb.toString());
			 listaMunicipios = q.list();
		 }catch(Exception e){
			 e.printStackTrace();
		 }finally{
			 session.close();
		 }
		 
		 return listaMunicipios;
	}
	
	public List<MunicipioAtendidoVO> preparaDadosMunicipio(Integer idRegiao,Integer idMunicipio,String exercicio){
		Map dados = new TreeMap();
		List<Object[]> listaMunicipios =this.sqlMunicipiosAtendidos(idRegiao,idMunicipio,exercicio);
		for (Object[] a : listaMunicipios) {
			if(dados.get(a[0])!=null){
				HashMap dad = (HashMap)dados.get(a[0]);
				dad.put(a[2].toString(), a[1].toString());
				dados.put(a[0].toString(), dad);
			}else{
				Map valores = new HashMap();
				valores.put(a[2].toString(), a[1].toString());
				dados.put(a[0].toString(), valores);
			}
		}
		
			List<MunicipioAtendidoVO> listaMunicipiosVO = new ArrayList<MunicipioAtendidoVO>();
			Set keys = dados.keySet();
		   for (Iterator i = keys.iterator(); i.hasNext();) {
			   MunicipioAtendidoVO vo = new MunicipioAtendidoVO();
			   Integer totalGeral = 0;
			   String key = (String) i.next();
			   HashMap value = (HashMap) dados.get(key);
			   vo.setNome(key);
			   if(value.containsKey("ATER Normal")){
				   vo.setTotalAterNormal(Integer.valueOf(value.get("ATER Normal").toString()));  
				   totalGeral = totalGeral + Integer.valueOf(value.get("ATER Normal").toString());
			   }
			   if(value.containsKey("Chamada P�blica")){
				   vo.setTotalChamadaPublica(Integer.valueOf(value.get("Chamada P�blica").toString()));
				   totalGeral = totalGeral + Integer.valueOf(value.get("Chamada P�blica").toString());
			   }
			   if(value.containsKey("MDA/ATER")){
				   vo.setTotalMDA(Integer.valueOf(value.get("MDA/ATER").toString()));
				   totalGeral = totalGeral + Integer.valueOf(value.get("MDA/ATER").toString());
			   }
			   vo.setTotalGeral(totalGeral);
			  
			   listaMunicipiosVO.add(vo);
		   }
		   
		   return listaMunicipiosVO;
	}
	
	public List<AssentamentoAtendidoVO> preparaDadosAssentamento(Integer idRegiao,Integer idMunicipio,String exercicio){
		Map dados = new TreeMap();
		List<Object[]> listaAssentamentos =this.sqlAssentamentoAtendido(idRegiao,idMunicipio,exercicio);
		for (Object[] a : listaAssentamentos) {
			if(dados.get(a[0])!=null){
				HashMap dad = (HashMap)dados.get(a[0]);
				dad.put(a[2].toString(), a[1].toString());
				dados.put(a[0].toString(), dad);
			}else{
				Map valores = new HashMap();
				valores.put(a[2].toString(), a[1].toString());
				dados.put(a[0].toString(), valores);
			}
		}
		
			List<AssentamentoAtendidoVO> listaAssentamentosVO = new ArrayList<AssentamentoAtendidoVO>();
			Set keys = dados.keySet();
		   for (Iterator i = keys.iterator(); i.hasNext();) {
			   AssentamentoAtendidoVO vo = new AssentamentoAtendidoVO();
			   Integer totalGeral = 0;
			   String key = (String) i.next();
			   HashMap value = (HashMap) dados.get(key);
			   vo.setNome(key);
			   if(value.containsKey("ATER Normal")){
				   vo.setTotalAterNormal(Integer.valueOf(value.get("ATER Normal").toString()));  
				   totalGeral = totalGeral + Integer.valueOf(value.get("ATER Normal").toString());
			   }
			   if(value.containsKey("Chamada P�blica")){
				   vo.setTotalChamadaPublica(Integer.valueOf(value.get("Chamada P�blica").toString()));
				   totalGeral = totalGeral + Integer.valueOf(value.get("Chamada P�blica").toString());
			   }
			   if(value.containsKey("MDA/ATER")){
				   vo.setTotalMDA(Integer.valueOf(value.get("MDA/ATER").toString()));
				   totalGeral = totalGeral + Integer.valueOf(value.get("MDA/ATER").toString());
			   }
			   vo.setTotalGeral(totalGeral);
			  
			   listaAssentamentosVO.add(vo);
		   }
		   
		   return listaAssentamentosVO;
	}

}
