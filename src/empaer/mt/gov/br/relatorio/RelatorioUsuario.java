package empaer.mt.gov.br.relatorio;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import empaer.mt.gov.br.dao.UsuarioDAO;
import empaer.mt.gov.br.model.UsuarioVO;



public class RelatorioUsuario {
	
	public void gerarRelatorio()throws JRException, IOException{
		UsuarioDAO dao = new UsuarioDAO();
		List<UsuarioVO> lista = new ArrayList<UsuarioVO>();
		List<Object[]> listaUsuario = dao.listUsuario();
		for (Object[] a : listaUsuario) {
			UsuarioVO vo = new UsuarioVO();
			vo.setLogin(a[0].toString());
			vo.setNome(a[1].toString());
			lista.add(vo);
		}
		
		 try{
				FacesContext context = FacesContext.getCurrentInstance();  
		        HttpServletResponse response = (HttpServletResponse) context  
		        .getExternalContext().getResponse();  
		  
		         
		        ServletOutputStream responseStream = response.getOutputStream();
		        ClassLoader classLoader = getClass().getClassLoader();    
		         
		        InputStream in = getClass().getResourceAsStream("empaer/mt/gov/br/relatorio/usuario.jrxml"); 
		        InputStream ino = getClass().getResourceAsStream("usuario.jrxml"); 
		        
		        
		        String arquivo1 = FacesContext.getCurrentInstance().getExternalContext().getRealPath("WEB-INF"+ File.separator + "imagens" + File.separator + "LOGO_EMPAER _2015.jpg");  
		        File nomearquivo = new File(arquivo1);     
		        
		       
		        
		        
		        // envia a resposta com o MIME Type  
		        response.setContentType("application/pdf");  
		  
		       
		        String nomeRel = "Relatorio de Usu�rios";
		        response.setHeader("Content-Disposition", "attachment; filename="+ nomeRel +".pdf");
		    
		        response.setContentType("application/pdf");
		        response.setHeader("Pragma", "no-cache");
		        
		        JasperReport pathReport = JasperCompileManager.compileReport(ino);
		        
		        Map parameters = new HashMap();     
		        parameters.put("LOGO",nomearquivo);

		      //relatorio gerado

		      JasperPrint preencher = JasperFillManager.fillReport(pathReport, parameters,new JRBeanCollectionDataSource(lista));
		    
		      
		      
		      JasperExportManager.exportReportToPdfStream(preencher,responseStream);
		      

		     
		      
			      responseStream.flush();
			
			      responseStream.close();
			
			      context.renderResponse();
			
			      context.responseComplete();
		       }catch(Exception e){
		    	   e.printStackTrace();
		    	   
		       }
	}

}
