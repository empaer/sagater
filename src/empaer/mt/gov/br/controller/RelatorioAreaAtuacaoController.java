package empaer.mt.gov.br.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import net.sf.jasperreports.engine.JRException;

import org.primefaces.event.SelectEvent;

import empaer.mt.gov.br.dao.MunicipioDAO;
import empaer.mt.gov.br.model.Municipio;
import empaer.mt.gov.br.model.Regiao;
import empaer.mt.gov.br.relatorio.RelatorioAreaAtuacao;

@ManagedBean(name="areaAtuacaoMB")
@ViewScoped
public class RelatorioAreaAtuacaoController implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 115987426L;
	private String exercicio;
	
	private String regional;
	
	private Regiao regiao;
	
	private Municipio municipio;
	
	private List<Municipio> listMunicipio;
	
	private List<Regiao> listRegiao;
	
	private Integer idRegiao;
	
	private Integer idCidade;
	
	public RelatorioAreaAtuacaoController(){
		listMunicipio = new ArrayList<Municipio>();
		listRegiao = new ArrayList<Regiao>();
		MunicipioDAO dao = new MunicipioDAO();
		listMunicipio = dao.listMunicipioBySigla("MT");
		listRegiao = dao.listRegiao();
		exercicio = "";
	}
	
	
	public void emitirRelatorio(){
		RelatorioAreaAtuacao relatorio = new RelatorioAreaAtuacao();
		try {
			relatorio.gerarRelatorio(this.idRegiao,this.idCidade,this.exercicio);
		} catch (JRException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getExercicio() {
		return exercicio;
	}
	public void setExercicio(String exercicio) {
		this.exercicio = exercicio;
	}
	public String getRegional() {
		return regional;
	}
	public void setRegional(String regional) {
		this.regional = regional;
	}
	public Municipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	public List<Municipio> getListMunicipio() {
		return listMunicipio;
	}
	public void setListMunicipio(List<Municipio> listMunicipio) {
		this.listMunicipio = listMunicipio;
	}


	public Regiao getRegiao() {
		return regiao;
	}


	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}


	public List<Regiao> getListRegiao() {
		return listRegiao;
	}


	public void setListRegiao(List<Regiao> listRegiao) {
		this.listRegiao = listRegiao;
	}
	
	
	public void carregaMunicipioByRegiao(){
		listMunicipio = new ArrayList<Municipio>();
		MunicipioDAO dao = new MunicipioDAO();
		listMunicipio = dao.listMunicipioByRegiao(this.idRegiao);
		
				
	}
	
//	public void onChange(SelectEvent event){
//		MunicipioDAO dao = new MunicipioDAO();
//		listMunicipio = dao.listMunicipioByRegiao(this.regiao);
//	}


	public Integer getIdRegiao() {
		return idRegiao;
	}


	public void setIdRegiao(Integer idRegiao) {
		this.idRegiao = idRegiao;
	}


	public Integer getIdCidade() {
		return idCidade;
	}


	public void setIdCidade(Integer idCidade) {
		this.idCidade = idCidade;
	}
	

}
