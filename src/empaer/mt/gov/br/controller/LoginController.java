package empaer.mt.gov.br.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;

import empaer.mt.gov.br.dao.UsuarioDAO;
import empaer.mt.gov.br.model.Escritorio;
import empaer.mt.gov.br.model.Usuario;
import empaer.mt.gov.br.util.Util;


@ManagedBean(name="loginMB")
@SessionScoped
public class LoginController implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 14545L;

	private String usuario;
	
	private String senha;
	
	private Usuario usuarioVO;
	
	private Escritorio escritorioSelecionado;
	
	private List<Escritorio> listaEscritorio;

	public LoginController(){
		this.usuario="";
		this.senha="";
		listaEscritorio = new ArrayList<Escritorio>();
		escritorioSelecionado=null;
//		HttpSession hs = Util.getSession();
//		if(hs!=null){
//			
//			
//			String idUsuario = hs.getAttribute("idUsuario").toString();
//			UsuarioDAO usuarioDAO = new UsuarioDAO();
//			usuarioVO = usuarioDAO.usuarioById(Integer.valueOf(idUsuario));
//			listaEscritorio = usuarioDAO.listEscritorioByIdUsuario(Integer.valueOf(idUsuario));
//		}
		
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	
	public Usuario getUsuarioVO() {
		return usuarioVO;
	}

	public void setUsuarioVO(Usuario usuarioVO) {
		this.usuarioVO = usuarioVO;
	}
	
	

	public Escritorio getEscritorioSelecionado() {
		return escritorioSelecionado;
	}

	public void setEscritorioSelecionado(Escritorio escritorioSelecionado) {
		this.escritorioSelecionado = escritorioSelecionado;
	}
	
	

	public List<Escritorio> getListaEscritorio() {
		return listaEscritorio;
	}

	public void setListaEscritorio(List<Escritorio> listaEscritorio) {
		this.listaEscritorio = listaEscritorio;
	}

	public String doLogin() {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean loggedIn = false;
        UsuarioDAO usuarioDAO = new UsuarioDAO();
        String senhaCrip = Util.criptografarSenha(this.getSenha());
        usuarioVO = usuarioDAO.usurioByLoginBySenha(this.getUsuario(), "38b3eff8baf56627478ec76a704e9b52");
        
        if(usuarioVO != null) {
        	HttpSession hs = Util.getSession();
			hs.setAttribute("idUsuario", usuarioVO.getIdUsuario());
			
			listaEscritorio = usuarioDAO.listEscritorioByIdUsuario(Integer.valueOf(usuarioVO.getIdUsuario()));
			//hs.setAttribute("perfil", login.getPerfil().getNomePerfil());
			return "paginas/selecao_escritorio.xhtml";
            
        } else {
            loggedIn = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Usu�rio ou Senha inv�lido!",null);
            return "login.xhtml";
        }
         
        
    } 
	
	public String selecaoEscritorio() throws IOException{
		
		HttpSession hs = Util.getSession();
		//hs.setAttribute("idUsuario", usuario.getIdUsuario());
		//hs.setAttribute("perfil", login.getPerfil().getNomePerfil());
		//FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
		hs.setAttribute("idEscritorio", this.escritorioSelecionado.getIdEscritorio());
		return "/index.xhtml?faces-redirect=true";  
		
	}
	
	public String doLogout(){
		HttpSession hs = Util.getSession();
		hs.invalidate();
		return "/login.xhtml";
	}
	

}
