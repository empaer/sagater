package empaer.mt.gov.br.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.LazyDataModel;

import empaer.mt.gov.br.dao.AgrupamentoDAO;
import empaer.mt.gov.br.dao.MunicipioDAO;
import empaer.mt.gov.br.dao.PessoaDAO;
import empaer.mt.gov.br.lazy.LazyAgrupamentoDataModel;
import empaer.mt.gov.br.model.Agrupamento;
import empaer.mt.gov.br.model.Municipio;
import empaer.mt.gov.br.model.Pessoa;



@ManagedBean(name="comunidadeMB")
@ViewScoped
public class ComunidadeController implements Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 173695L;
	private Agrupamento agrupamento;
	
	private List<Municipio> listMunicipio;
	
	private LazyDataModel<Agrupamento> lazyModel;
	
	private List<Agrupamento> listaAgrupamento;
	
	private List<Agrupamento> agrupamentoFiltrados;
	
	private Agrupamento agrupamentoSelecionado;

	public ComunidadeController(){
		agrupamento = new Agrupamento();
		listMunicipio = new ArrayList<Municipio>();
		MunicipioDAO dao = new MunicipioDAO();
		listMunicipio = dao.listMunicipioBySigla("MT");
		this.popularPesquisa();
	}

	public Agrupamento getAgrupamento() {
		return agrupamento;
	}

	public void setAgrupamento(Agrupamento agrupamento) {
		this.agrupamento = agrupamento;
	}

	public List<Municipio> getListMunicipio() {
		return listMunicipio;
	}

	public void setListMunicipio(List<Municipio> listMunicipio) {
		this.listMunicipio = listMunicipio;
	}
	
	
		
	public LazyDataModel<Agrupamento> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Agrupamento> lazyModel) {
		this.lazyModel = lazyModel;
	}

	public List<Agrupamento> getListaAgrupamento() {
		return listaAgrupamento;
	}

	public void setListaAgrupamento(List<Agrupamento> listaAgrupamento) {
		this.listaAgrupamento = listaAgrupamento;
	}

	public List<Agrupamento> getAgrupamentoFiltrados() {
		return agrupamentoFiltrados;
	}

	public void setAgrupamentoFiltrados(List<Agrupamento> agrupamentoFiltrados) {
		this.agrupamentoFiltrados = agrupamentoFiltrados;
	}

	public Agrupamento getAgrupamentoSelecionado() {
		return agrupamentoSelecionado;
	}

	public void setAgrupamentoSelecionado(Agrupamento agrupamentoSelecionado) {
		this.agrupamentoSelecionado = agrupamentoSelecionado;
	}

	public void salvar(){
		AgrupamentoDAO dao = new AgrupamentoDAO();
		try{
			
			this.agrupamento.setTipo("C");
			dao.salvarAtualizar(this.agrupamento);
			FacesMessage fm = new FacesMessage("Cadastro realizado com sucesso.","SUCESSO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			this.agrupamento = new Agrupamento();
		}catch(Exception e){
			FacesMessage fm = new FacesMessage("Houve um erro e o registro n�o foi gravado.","ERRO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void popularPesquisa(){
		AgrupamentoDAO  pessoaDAO = new AgrupamentoDAO();
		this.listaAgrupamento = pessoaDAO.listAgrupamento("C");
        lazyModel = new LazyAgrupamentoDataModel(listaAgrupamento);
        agrupamentoFiltrados = new ArrayList<Agrupamento>();
	}
	
	public void alterarComunidade(){
		this.agrupamento = this.agrupamentoSelecionado;
	}
	
	public void alterar(){
		AgrupamentoDAO dao = new AgrupamentoDAO();
		try{
			dao.salvarAtualizar(this.agrupamento);
			FacesMessage fm = new FacesMessage("Altera��o realizada com sucesso.","SUCESSO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			this.agrupamento = new Agrupamento();
		}catch(Exception e){
			e.printStackTrace();
			FacesMessage fm = new FacesMessage("Houve um erro e o registro n�o foi gravado.","ERRO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void excluir(){
		AgrupamentoDAO dao = new  AgrupamentoDAO();
		
		try{
			
			dao.excluir(this.agrupamento);
			FacesMessage fm = new FacesMessage("Exclus�o realizada com sucesso.","SUCESSO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			this.agrupamento = new Agrupamento();
		}catch(Exception e){
			e.printStackTrace();
			FacesMessage fm = new FacesMessage("Existe rela��o com outros registro do sistema, n�o pode ser excluido.","ERRO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void novo(){
		this.agrupamento = new Agrupamento();
	}
	
}
