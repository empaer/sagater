package empaer.mt.gov.br.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;

import empaer.mt.gov.br.dao.EscritorioDAO;
import empaer.mt.gov.br.dao.MunicipioDAO;
import empaer.mt.gov.br.dao.PessoaDAO;
import empaer.mt.gov.br.lazy.LazyPessoaDataModel;
import empaer.mt.gov.br.model.Escritorio;
import empaer.mt.gov.br.model.Municipio;
import empaer.mt.gov.br.model.Pessoa;
import empaer.mt.gov.br.util.Util;
import empaer.mt.gov.br.util.ValidaCPF;

@ManagedBean(name="pessoaMB")
@ViewScoped
public class PessoaController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 14876453L;

	private Pessoa pessoa;
	
	private List<Municipio> listMunicipio;
	
	private LazyDataModel<Pessoa> lazyModel;
	
	private List<Pessoa> listaPessoa;
	
	private Pessoa pessoaSelecionada;
	
	private List<Pessoa> pessoasFiltradas;
	
	
	public PessoaController(){
		pessoa = new Pessoa();
		listMunicipio = new ArrayList<Municipio>();
		MunicipioDAO dao = new MunicipioDAO();
		listMunicipio = dao.listMunicipioBySigla("MT");
		this.popularPesquisa();
		
		
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	
	
	public List<Municipio> getListMunicipio() {
		return listMunicipio;
	}

	public void setListMunicipio(List<Municipio> listMunicipio) {
		this.listMunicipio = listMunicipio;
	}
	
	

	public LazyDataModel<Pessoa> getLazyModel() {
		return lazyModel;
	}

	public void setLazyModel(LazyDataModel<Pessoa> lazyModel) {
		this.lazyModel = lazyModel;
	}
	
	

	public List<Pessoa> getListaPessoa() {
		return listaPessoa;
	}

	public void setListaPessoa(List<Pessoa> listaPessoa) {
		this.listaPessoa = listaPessoa;
	}
	
	
	public Pessoa getPessoaSelecionada() {
		return pessoaSelecionada;
	}

	public void setPessoaSelecionada(Pessoa pessoaSelecionada) {
		this.pessoaSelecionada = pessoaSelecionada;
	}

	
	public List<Pessoa> getPessoasFiltradas() {
		return pessoasFiltradas;
	}

	public void setPessoasFiltradas(List<Pessoa> pessoasFiltradas) {
		this.pessoasFiltradas = pessoasFiltradas;
	}

	public void salvar(){
		PessoaDAO dao = new  PessoaDAO();
		String cpfSemMascara = Util.cpfSemMascara(this.pessoa.getCpf());
		Pessoa pessoaTemp = dao.pesquisarPessoaPorCPF(cpfSemMascara);
		
		if(pessoaTemp!=null){
			FacesMessage fm = new FacesMessage("CPF j� cadastrado.","WARNING MSG");
			fm.setSeverity(FacesMessage.SEVERITY_WARN);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			return;
		}
		
		
		if(!ValidaCPF.isCPF(cpfSemMascara)){
			
			FacesMessage fm = new FacesMessage("CPF Inv�lido.","WARNING MSG");
			fm.setSeverity(FacesMessage.SEVERITY_WARN);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			return;
		}
		
		HttpSession hs = Util.getSession();
		Escritorio escritorio = null;
		if(hs!=null){
			String idEscritorio = hs.getAttribute("idEscritorio").toString();
			EscritorioDAO escDAO = new EscritorioDAO();
			escritorio = escDAO.findEscritorioByID(Integer.valueOf(idEscritorio));
		}
		
		
		
		try{
			
			this.pessoa.setCpf(cpfSemMascara);
			this.pessoa.setEscritorio(escritorio);
			this.pessoa.setRelacaoChefe("C");
			dao.salvarAtualizar(this.pessoa);
			FacesMessage fm = new FacesMessage("Cadastro realizado com sucesso.","SUCESSO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			pessoa = new Pessoa();
		}catch(Exception e){
			FacesMessage fm = new FacesMessage("Houve um erro e o registro n�o foi gravado.","ERRO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void popularPesquisa(){
		PessoaDAO  pessoaDAO = new PessoaDAO();
		this.listaPessoa = pessoaDAO.listPessoa();
        lazyModel = new LazyPessoaDataModel(listaPessoa);
        pessoasFiltradas = new ArrayList<Pessoa>();
	}
	
	public void alterar(){
		this.pessoa= this.pessoaSelecionada;
	}
	
	public void novo(){
		this.pessoa= new Pessoa();
	}
	
	public void alterarPessoa(){
		PessoaDAO dao = new  PessoaDAO();
		String cpfSemMascara = Util.cpfSemMascara(this.pessoa.getCpf());
		try{
			this.pessoa.setCpf(cpfSemMascara);
			dao.salvarAtualizar(this.pessoa);
			FacesMessage fm = new FacesMessage("Altera��o realizada com sucesso.","SUCESSO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			this.pessoa = new Pessoa();
		}catch(Exception e){
			e.printStackTrace();
			FacesMessage fm = new FacesMessage("Houve um erro e o registro n�o foi gravado.","ERRO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	public void excluir(){
		PessoaDAO dao = new  PessoaDAO();
		
		try{
			
			dao.excluir(this.pessoa);
			FacesMessage fm = new FacesMessage("Exclus�o realizada com sucesso.","SUCESSO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			FacesContext.getCurrentInstance().addMessage(null, fm);
			this.pessoa = new Pessoa();
		}catch(Exception e){
			e.printStackTrace();
			FacesMessage fm = new FacesMessage("Existe rela��o com outros registro do sistema, n�o pode ser excluido.","ERRO MSG");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			FacesContext.getCurrentInstance().addMessage(null, fm);
		}
	}
	
	
	public void onTabChange(TabChangeEvent event) {
		//String titulo = event.getTab().getTitle().toString();
		TabView tv = (TabView) event.getComponent(); 
		Integer numrAba = tv.getActiveIndex();
		if(numrAba==2){
			this.popularPesquisa();
		}
        //FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle().getTitle());
       // FacesContext.getCurrentInstance().addMessage(null, msg);
    }
	
	
	
	
}
