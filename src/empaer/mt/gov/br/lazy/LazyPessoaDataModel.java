package empaer.mt.gov.br.lazy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import empaer.mt.gov.br.dao.PessoaDAO;
import empaer.mt.gov.br.model.Pessoa;

public class LazyPessoaDataModel extends LazyDataModel<Pessoa>{

	private static final long serialVersionUID = 1456546L;
	private List<Pessoa> datasource;
	
	public LazyPessoaDataModel(List<Pessoa> datasource) {
        this.datasource = datasource;
    }
	
	
	@Override
    public Pessoa getRowData(String rowKey) {
        for(Pessoa ficha : datasource) {
            if(ficha.getIdPessoa().equals(rowKey))
                return ficha;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Pessoa pessoa) {
        return pessoa.getIdPessoa();
    }
 
    @Override
    public List<Pessoa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Pessoa> data = new ArrayList<Pessoa>();
        PessoaDAO dao = new PessoaDAO();
        //filter
        int cont = 0;
        for(Pessoa car : datasource) {
            boolean match = true;
            cont++;
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                    	System.out.println("filters");

                        String filterProperty = it.next();
                        System.out.println("filterProperty: " + filterProperty);

                        String filterValue = (String) filters.get(filterProperty);
                        System.out.println("filterValue: " + filterValue);
                        if(cont==1){
                        	data = dao.pesquisaGenerica(filterProperty, filterValue);
                        }
                        //System.out.println("load: " + car.getIdFichaFuncional());
                        //System.out.println(car.getClass().getFields()[0].getName());
                        //System.out.println(car.getClass().getField(filterProperty).getName());
                        //Class.forName(this.getClass().getName()).getDeclaredField(filterProperty);

                       // String fieldValue = String.valueOf(car.getClass().getField(filterProperty).get(car));
                        //System.out.println("fieldValue: " + fieldValue);

                        if(data.isEmpty()) {
                            match = true;
                        }
                        else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                    	e.printStackTrace();
                        match = false;
                    }
                }
            }
 
            if(match && filters.isEmpty()) {
                data.add(car);
            }
        }
 
        //sort
        if(sortField != null) {
        	Collections.sort(data, new LazySorterPessoa(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }


	public List<Pessoa> getDatasource() {
		return datasource;
	}


	public void setDatasource(List<Pessoa> datasource) {
		this.datasource = datasource;
	}

}
