package empaer.mt.gov.br.lazy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import empaer.mt.gov.br.dao.AgrupamentoDAO;
import empaer.mt.gov.br.model.Agrupamento;


public class LazyAgrupamentoDataModel extends LazyDataModel<Agrupamento>{
	
	private static final long serialVersionUID = 1456546L;
	private List<Agrupamento> datasource;
	
	public LazyAgrupamentoDataModel(List<Agrupamento> datasource) {
        this.datasource = datasource;
    }
	
	
	@Override
    public Agrupamento getRowData(String rowKey) {
        for(Agrupamento ficha : datasource) {
            if(ficha.getIdAgrupamento().equals(rowKey))
                return ficha;
        }
 
        return null;
    }
 
    @Override
    public Object getRowKey(Agrupamento agrupamento) {
        return agrupamento.getIdAgrupamento();
    }
 
    @Override
    public List<Agrupamento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        List<Agrupamento> data = new ArrayList<Agrupamento>();
        AgrupamentoDAO dao = new AgrupamentoDAO();
        //filter
        int cont = 0;
        for(Agrupamento car : datasource) {
            boolean match = true;
            cont++;
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                    	System.out.println("filters");

                        String filterProperty = it.next();
                        System.out.println("filterProperty: " + filterProperty);

                        String filterValue = (String) filters.get(filterProperty);
                        System.out.println("filterValue: " + filterValue);
                        if(cont==1){
                        	if(car.getTipo().equals("C")){
                        		data = dao.pesquisaGenerica(filterProperty, filterValue);
                        	}else if(car.getTipo().equals("A")){
                        		data = dao.pesquisaGenericaAssentamento(filterProperty, filterValue);
                        	}
                        }
                        //System.out.println("load: " + car.getIdFichaFuncional());
                        //System.out.println(car.getClass().getFields()[0].getName());
                        //System.out.println(car.getClass().getField(filterProperty).getName());
                        //Class.forName(this.getClass().getName()).getDeclaredField(filterProperty);

                       // String fieldValue = String.valueOf(car.getClass().getField(filterProperty).get(car));
                        //System.out.println("fieldValue: " + fieldValue);

                        if(data.isEmpty()) {
                            match = true;
                        }
                        else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                    	e.printStackTrace();
                        match = false;
                    }
                }
            }
 
            if(match && filters.isEmpty()) {
                data.add(car);
            }
        }
 
        //sort
        if(sortField != null) {
        	Collections.sort(data, new LazySorterAgrupamento(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }


	public List<Agrupamento> getDatasource() {
		return datasource;
	}


	public void setDatasource(List<Agrupamento> datasource) {
		this.datasource = datasource;
	}

}
