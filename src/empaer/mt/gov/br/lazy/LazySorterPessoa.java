package empaer.mt.gov.br.lazy;

import java.lang.reflect.Field;
import java.util.Comparator;

import org.primefaces.model.SortOrder;


import empaer.mt.gov.br.model.Pessoa;



public class LazySorterPessoa implements Comparator<Pessoa>{
	
	private String sortField;
    
    private SortOrder sortOrder;

    
	public LazySorterPessoa(String sortField, SortOrder sortOrder) {
	  this.sortField = sortField;
	  this.sortOrder = sortOrder;
	}

	@Override
	public int compare(Pessoa arg0, Pessoa arg1) {
	 try {
	        //Object value1 = FichaFuncional.class.getField(this.sortField).get(arg0);
	      //  Object value2 = FichaFuncional.class.getField(this.sortField).get(arg1);
	
	        //int value = ((Comparable)value1).compareTo(value2);
	         
	       // return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
	        
	        
	        Field field1 = arg0.getClass().getDeclaredField(this.sortField);
	        Field field2 = arg1.getClass().getDeclaredField(this.sortField);
	        field1.setAccessible(true);
	        field2.setAccessible(true);
	        Object value1 = field1.get(arg0);
	        Object value2 = field2.get(arg1);
	      
	        	int value = ((Comparable)value1).compareTo(value2);
	        
	        	return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
	       
	        	
	        
	    }
	    catch(Exception e) {
	        throw new RuntimeException();
	    }
	}


}
