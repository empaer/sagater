package empaer.mt.gov.br.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class FiltroSessao implements Filter{
	
	@Override 
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //Verifica se a sess�o n�o expirou, se sim volta para a p�gina de login
        HttpSession session = ((HttpServletRequest)request).getSession(false);
        if(session != null && !session.isNew() && session.getAttribute("idUsuario")!=null) {
            chain.doFilter(request, response);
        } else {
            //Retorna para a p�gina de login
        	((HttpServletResponse)response).sendRedirect(((HttpServletRequest)request).getContextPath()+"/login.xhtml");
        }
    }

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
