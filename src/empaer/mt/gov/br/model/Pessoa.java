package empaer.mt.gov.br.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import empaer.mt.gov.br.util.Util;


@Entity
@Table(name="emptb_008_pessoa")
@SequenceGenerator(name="emptb_008_pessoa_id_pessoa_seq", sequenceName="emptb_008_pessoa_id_pessoa_seq", allocationSize=1)
public class Pessoa implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 18963354L;

	@Id
	@Column(name = "id_pessoa", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emptb_008_pessoa_id_pessoa_seq")
	private Integer idPessoa;
	
	@Column(name="pes_nome", length=150)
	private String nome;
	
	
	@Column(name="pes_cpf", length=14)
	private String cpf;
	
	@Column(name="pes_rg", length=15)
	private String rg;
	
	@Column(name="pes_data_nascimento")
	private Date dataNascimento;
	
	@Column(name="pes_sexo", length=1) //M-Masculino; F-Feminino
	private String sexo;
	
	@Column(name="pes_estado_civil", length=1) //S - Solteiro(a); C - Casado(a); U - Uni�o Est�vel; D - Divorciado(a); V - Vi�vo(a);
	private String estadoCivil;
	
	@Column(name="pes_escolaridade") //1 - Fundamental Incompleto; 2 - Fundamental Completo; 3 - Ensino M�dio Incompleto; 4 - Ensino M�dio Completo; 5 - Superior Incompleto; 6 - Superior Completo; 7 - Especializa��o; 8 - Mestrado; 9 - Doutorado
	private Integer escolaridade;
	
	@Column(name="pes_apelido", length=150)
	private String apelido;
	
	@Column(name="pes_relacao_chefe", length=1)
	private String relacaoChefe; //F - Filho(a); M - C�njuge; C - Chefe
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_chefe_familia", insertable=true, updatable=true)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
    private Pessoa chefeFamilia;
	
	
	@Column(name="pes_telefone", length=20)
	private String telefone;
	
	@Column(name="pes_telefone_celular", length=20)
	private String celular;
	
	@Column(name="pes_telefone_comercial", length=20)
	private String telefoneComercial;
	
	@Column(name="pes_logradouro", length=200)
	private String logradouro;
	
	@Column(name="pes_numero", length=20)
	private String numero;
	
	@Column(name="pes_complemento", length=100)
	private String complemento;
	
	@Column(name="pes_bairro", length=200)
	private String bairro;
	
	@ManyToOne()
	@JoinColumn(name="id_escritorio")
	@Cascade(CascadeType.SAVE_UPDATE)
	private Escritorio escritorio;
	
	@ManyToOne()
	@JoinColumn(name="id_municipio")
	@Cascade(CascadeType.SAVE_UPDATE)
	private Municipio municipio;

	public Integer getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getNome() {
		return nome!=null ?nome.toUpperCase():"";
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}
	
	public String getCpfFormatado() {
		return cpf!=null ? Util.cpfComMascara(cpf):"";
	}
	

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg!=null ? rg :"";
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Integer getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(Integer escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getApelido() {
		return apelido!=null ? apelido: "";
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getRelacaoChefe() {
		return relacaoChefe;
	}
	
	public String getRelacaoChefeFormatada(){
		String relacao="";
		
		if(this.relacaoChefe.equals("C")){
			relacao="Chefe";
		}else if(this.relacaoChefe.equals("F")){
			relacao="Filho";
		}else if(this.relacaoChefe.equals("M")){
			relacao="Conjug�";
		}
		
		return relacao;
	}

	public void setRelacaoChefe(String relacaoChefe) {
		this.relacaoChefe = relacaoChefe;
	}

	public Pessoa getChefeFamilia() {
		return chefeFamilia;
	}

	public void setChefeFamilia(Pessoa chefeFamilia) {
		this.chefeFamilia = chefeFamilia;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefoneComercial() {
		return telefoneComercial;
	}

	public void setTelefoneComercial(String telefoneComercial) {
		this.telefoneComercial = telefoneComercial;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Escritorio getEscritorio() {
		return escritorio;
	}

	public void setEscritorio(Escritorio escritorio) {
		this.escritorio = escritorio;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	

	
	

	
	

}
