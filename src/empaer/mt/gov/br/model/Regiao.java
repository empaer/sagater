package empaer.mt.gov.br.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import empaer.mt.gov.br.util.SampleEntity;

@Entity
@Table(name="emptb_045_regiao")
@SequenceGenerator(name="emptb_045_regiao_id_regiao_seq", sequenceName="emptb_045_regiao_id_regiao_seq", allocationSize=1)
public class Regiao implements Serializable,SampleEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id_regiao", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emptb_045_regiao_id_regiao_seq")
	private Integer idRegiao;
	
	@Column(name="reg_descricao", length=100)
	private String descRegiao;

	public Integer getIdRegiao() {
		return idRegiao;
	}

	public void setIdRegiao(Integer idRegiao) {
		this.idRegiao = idRegiao;
	}

	public String getDescRegiao() {
		return descRegiao;
	}

	public void setDescRegiao(String descRegiao) {
		this.descRegiao = descRegiao;
	}

	public boolean equals(Object o){

		  if (o == null)  return false;  
	        if (o instanceof Municipio){
	            return ((Regiao)o).getIdRegiao().equals(this.idRegiao);  
	        }
	        return false;

	}

	  
	  public int hashCode() {
		  return (idRegiao == null) ? 0 : idRegiao;
		  }
	  
	  @Override
		public Long getId() {
			if(this.getIdRegiao()!=null){
				return this.getIdRegiao().longValue();
				}else{
					return null;
				}
		}
	
}
