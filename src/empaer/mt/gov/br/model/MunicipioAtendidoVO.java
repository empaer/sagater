package empaer.mt.gov.br.model;

public class MunicipioAtendidoVO {
	
	private String nome;
	
	private Integer numrAtendimento;
	
	private Integer totalChamadaPublica;
	
	private Integer totalMDA;
	
	private Integer totalAterNormal;
	
	private Integer totalGeral;
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	

	public Integer getNumrAtendimento() {
		return numrAtendimento;
	}

	public void setNumrAtendimento(Integer numrAtendimento) {
		this.numrAtendimento = numrAtendimento;
	}

	public Integer getTotalChamadaPublica() {
		return totalChamadaPublica != null ? totalChamadaPublica:0;
	}

	public void setTotalChamadaPublica(Integer totalChamadaPublica) {
		this.totalChamadaPublica = totalChamadaPublica;
	}

	public Integer getTotalMDA() {
		return totalMDA != null ? totalMDA:0;
	}

	public void setTotalMDA(Integer totalMDA) {
		this.totalMDA = totalMDA;
	}

	public Integer getTotalAterNormal() {
		return totalAterNormal;
	}

	public void setTotalAterNormal(Integer totalAterNormal) {
		this.totalAterNormal = totalAterNormal;
	}

	public Integer getTotalGeral() {
		return totalGeral;
	}

	public void setTotalGeral(Integer totalGeral) {
		this.totalGeral = totalGeral;
	}
	
	

}
