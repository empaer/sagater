package empaer.mt.gov.br.model;

import java.util.List;

public class RelatorioAreaAtuacaoVO {
	
	private List<MunicipioAtendidoVO> listaMunicipioAtendido;
	
	private List<AssentamentoAtendidoVO> listaAssentamentoAtendido;

	public List<MunicipioAtendidoVO> getListaMunicipioAtendido() {
		return listaMunicipioAtendido;
	}

	public void setListaMunicipioAtendido(
			List<MunicipioAtendidoVO> listaMunicipioAtendido) {
		this.listaMunicipioAtendido = listaMunicipioAtendido;
	}

	public List<AssentamentoAtendidoVO> getListaAssentamentoAtendido() {
		return listaAssentamentoAtendido;
	}

	public void setListaAssentamentoAtendido(
			List<AssentamentoAtendidoVO> listaAssentamentoAtendido) {
		this.listaAssentamentoAtendido = listaAssentamentoAtendido;
	}
	
	
	
	
}
