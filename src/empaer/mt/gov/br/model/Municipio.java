package empaer.mt.gov.br.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import empaer.mt.gov.br.util.SampleEntity;

@Entity
@Table(name="emptb_014_municipio")
@SequenceGenerator(name="emptb_014_municipio_id_municipio_seq", sequenceName="emptb_014_municipio_id_municipio_seq", allocationSize=1)
public class Municipio implements Serializable,SampleEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 100004546L;

	@Id
	@Column(name = "id_municipio", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emptb_014_municipio_id_municipio_seq")
	private Integer idMunicipio;
	
	@Column(name="mun_nome", length=150)
	private String nomeMunicipio;
	
	@Column(name="mun_estado", length=2)
	private String siglaEstado;
	
	@ManyToOne()
	@JoinColumn(name="id_regiao")
	@Cascade(CascadeType.SAVE_UPDATE)
	private Regiao regiao;

	public Integer getIdMunicipio() {
		return idMunicipio;
	}

	public void setIdMunicipio(Integer idMunicipio) {
		this.idMunicipio = idMunicipio;
	}

	public String getNomeMunicipio() {
		return nomeMunicipio;
	}

	public void setNomeMunicipio(String nomeMunicipio) {
		this.nomeMunicipio = nomeMunicipio;
	}

	public String getSiglaEstado() {
		return siglaEstado;
	}

	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}
	
	public boolean equals(Object o){

		  if (o == null)  return false;  
	        if (o instanceof Municipio){
	            return ((Municipio)o).getIdMunicipio().equals(this.idMunicipio);  
	        }
	        return false;

	}

	  
	  public int hashCode() {
		  return (idMunicipio == null) ? 0 : idMunicipio;
		  }

	@Override
	public Long getId() {
		if(this.getIdMunicipio()!=null){
			return this.getIdMunicipio().longValue();
			}else{
				return null;
			}
	}
	
	


}
