package empaer.mt.gov.br.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;


@Embeddable
public class EscritorioUsuarioPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="usr_codigo")
	private Long usrCodigo;

	@Column(name="id_escritorio")
	private Integer idEscritorio;

	public EscritorioUsuarioPK() {
	}
	public Long getUsrCodigo() {
		return this.usrCodigo;
	}
	public void setUsrCodigo(Long usrCodigo) {
		this.usrCodigo = usrCodigo;
	}
	public Integer getIdEscritorio() {
		return this.idEscritorio;
	}
	public void setIdEscritorio(Integer idEscritorio) {
		this.idEscritorio = idEscritorio;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EscritorioUsuarioPK)) {
			return false;
		}
		EscritorioUsuarioPK castOther = (EscritorioUsuarioPK)other;
		return 
			this.usrCodigo.equals(castOther.usrCodigo)
			&& this.idEscritorio.equals(castOther.idEscritorio);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.usrCodigo.hashCode();
		hash = hash * prime + this.idEscritorio.hashCode();
		
		return hash;
	}
}
