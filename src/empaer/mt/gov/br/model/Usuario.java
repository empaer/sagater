package empaer.mt.gov.br.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="fr_usuario") 
@SequenceGenerator(name="fr_usuario_seq", sequenceName="fr_usuario_seq", allocationSize=1)
public class Usuario implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 145645645L;

	@Id
	@Column(name = "usr_codigo", nullable = false)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "fr_usuario_seq")
	private Integer idUsuario;
	
	@Column(name="usr_nome", length=60)
	private String nome;
	
	@Column(name="usr_senha", length=64)
	private String senha;
	
	@Column(name="usr_login", length=20)
	private String login;
	
	@Column(name="usr_administrador", length=1)
	private String administrador;
	
	@Column(name="usr_tipo_expiracao", length=1)
	private String tipoExpericao;
	
	@Column(name="usr_dias_expiracao")
	private Integer diasExperiracao;
	
	@Column(name="usr_email", length=120)
	private String email;
	
	@Column(name="urs_matricula", length=100)
	private String matricula;
	

//	//bi-directional many-to-one association to Emptb002EscritorioUsuario
//	@OneToMany(mappedBy="usuario",targetEntity =EscritorioUsuario.class,fetch = FetchType.EAGER)
//	private List<EscritorioUsuario> escritorioUsuarios;

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getAdministrador() {
		return administrador;
	}

	public void setAdministrador(String administrador) {
		this.administrador = administrador;
	}

	public String getTipoExpericao() {
		return tipoExpericao;
	}

	public void setTipoExpericao(String tipoExpericao) {
		this.tipoExpericao = tipoExpericao;
	}

	public Integer getDiasExperiracao() {
		return diasExperiracao;
	}

	public void setDiasExperiracao(Integer diasExperiracao) {
		this.diasExperiracao = diasExperiracao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

//	public List<EscritorioUsuario> getEscritorioUsuarios() {
//		return escritorioUsuarios;
//	}
//
//	public void setEscritorioUsuarios(List<EscritorioUsuario> escritorioUsuarios) {
//		this.escritorioUsuarios = escritorioUsuarios;
//	}
	
	

}
