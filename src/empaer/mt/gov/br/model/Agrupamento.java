package empaer.mt.gov.br.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="emptb_010_agrupamento")
@SequenceGenerator(name="emptb_010_agrupamento_id_agrupamento_seq", sequenceName="emptb_010_agrupamento_id_agrupamento_seq", allocationSize=1)
public class Agrupamento implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 15453543L;
	
	@Id
	@Column(name = "id_agrupamento", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emptb_010_agrupamento_id_agrupamento_seq")
	private Integer idAgrupamento;
	
	@Column(name="agru_tipo", length=1)
	private String tipo;
	
	
	@Column(name="agru_nome", length=150)
	private String nome;
	
	@Column(name="agru_caracteristica", length=2)
	private String caracteristica;
	
	@Column(name="agru_cnpj", length=28)
	private String cnpj;
	
	@Column(name="agru_telefone", length=21)
	private String telefone;
	
	@Column(name="agru_num_pessoas")
	private Integer numrPessoas;
	
	@ManyToOne()
	@JoinColumn(name="id_municipio")
	@Cascade(CascadeType.SAVE_UPDATE)
	private Municipio municipio;

	public Integer getIdAgrupamento() {
		return idAgrupamento;
	}

	public void setIdAgrupamento(Integer idAgrupamento) {
		this.idAgrupamento = idAgrupamento;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCaracteristica() {
		return caracteristica;
	}
	
	public String getCaracteristicaFormatada() {
		String caractFormatada="";
		if(this.caracteristica.equals("T")){
			caractFormatada = "Comunidade Tradicional";
		}
		
		if(this.caracteristica.equals("IN")){
			caractFormatada = "INCRA";
		}
		
		if(this.caracteristica.equals("IM")){
			caractFormatada = "INTERMAT";
		}
		
		if(this.caracteristica.equals("C")){
			caractFormatada = "CASULO";
		}
		
		if(this.caracteristica.equals("CF")){
			caractFormatada = "CR�DITO FUNDI�RIO";
		}
		
		return caractFormatada;
	}

	public void setCaracteristica(String caracteristica) {
		
		this.caracteristica = caracteristica;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Integer getNumrPessoas() {
		return numrPessoas;
	}

	public void setNumrPessoas(Integer numrPessoas) {
		this.numrPessoas = numrPessoas;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}
	
	
	

}
