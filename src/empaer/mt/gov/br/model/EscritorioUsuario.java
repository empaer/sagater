package empaer.mt.gov.br.model;


import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name="emptb_002_escritorio_usuario")
public class EscritorioUsuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 15456L;

	@EmbeddedId
	private EscritorioUsuarioPK id;

//	//bi-directional many-to-one association to Emptb001Escritorio
	@ManyToOne
	@JoinColumn(name="id_Escritorio",referencedColumnName="id_escritorio", insertable=false, updatable=false)
	private Escritorio escritorio;
	
	//bi-directional many-to-one association to Emptb001Escritorio
	@ManyToOne
	@JoinColumn(name="usr_codigo",referencedColumnName="usr_codigo", insertable=false, updatable=false)
	private Usuario usuario;
	
	
	public EscritorioUsuario() {
	}

	public EscritorioUsuarioPK getId() {
		return this.id;
	}

	public void setId(EscritorioUsuarioPK id) {
		this.id = id;
	}

	public Escritorio getEscritorio() {
		return this.escritorio;
	}

	public void setEscritorio(Escritorio escritorio) {
		this.escritorio = escritorio;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	

}
