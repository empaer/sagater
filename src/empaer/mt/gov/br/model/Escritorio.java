package empaer.mt.gov.br.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import empaer.mt.gov.br.util.SampleEntity;


@Entity
@Table(name="emptb_001_escritorio")
@SequenceGenerator(name="emptb_001_escritorio_id_escritorio_seq", sequenceName="emptb_001_escritorio_id_escritorio_seq", allocationSize=1)
public class Escritorio implements Serializable,SampleEntity{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 145456546L;

	@Id
	@Column(name = "id_escritorio", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emptb_001_escritorio_id_escritorio_seq")
	private Integer idEscritorio;
	
	@Column(name="esc_nome")
	private String nomeEscritorio;
	
	@Column(name="esc_endereco")
	private String enderecoEscritorio;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_escritorio_pai", insertable=true, updatable=true)
	@Fetch(FetchMode.JOIN)
	@Cascade(CascadeType.SAVE_UPDATE)
    private Escritorio escritorioPai;
	
	
//	@OneToMany(mappedBy="escritorio",targetEntity =Escritorio.class)
//	private List<Escritorio> escritorios;

	//bi-directional many-to-one association to Emptb002EscritorioUsuario
	@OneToMany(mappedBy="escritorio",targetEntity =EscritorioUsuario.class,fetch = FetchType.EAGER)
	private List<EscritorioUsuario> escritorioUsuarios;

	

//
//	public List<Escritorio> getEscritorios() {
//		return escritorios;
//	}
//
//	public void setEscritorios(List<Escritorio> escritorios) {
//		this.escritorios = escritorios;
//	}

	public List<EscritorioUsuario> getEscritorioUsuarios() {
		return escritorioUsuarios;
	}

	public void setEscritorioUsuarios(List<EscritorioUsuario> escritorioUsuarios) {
		this.escritorioUsuarios = escritorioUsuarios;
	}

	public Integer getIdEscritorio() {
		return idEscritorio;
	}

	public void setIdEscritorio(Integer idEscritorio) {
		this.idEscritorio = idEscritorio;
	}

	public String getNomeEscritorio() {
		return nomeEscritorio;
	}

	public void setNomeEscritorio(String nomeEscritorio) {
		this.nomeEscritorio = nomeEscritorio;
	}

	public String getEnderecoEscritorio() {
		return enderecoEscritorio;
	}

	public void setEnderecoEscritorio(String enderecoEscritorio) {
		this.enderecoEscritorio = enderecoEscritorio;
	}

	public Escritorio getEscritorioPai() {
		return escritorioPai;
	}

	public void setEscritorioPai(Escritorio escritorioPai) {
		this.escritorioPai = escritorioPai;
	}

	@Override
	public Long getId() {
		if(this.getIdEscritorio()!=null){
		return this.getIdEscritorio().longValue();
		}else{
			return null;
		}
	}
	
	  public boolean equals(Object o){

		  if (o == null)  return false;  
	        if (o instanceof Escritorio){
	            return ((Escritorio)o).getIdEscritorio().equals(this.idEscritorio);  
	        }
	        return false;

}

	  
	  public int hashCode() {
		  return (idEscritorio == null) ? 0 : idEscritorio;
		  }
	
	
	
	
	

}
