package empaer.mt.gov.br.util;

import java.security.MessageDigest;
import java.text.ParseException;
import java.util.Formatter;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.swing.text.MaskFormatter;


public class Util {

		
	public static String criptografarSenha(String original){
		String senha="";
		try{
			MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
			byte messageDigest[] = algorithm.digest(original.getBytes("UTF-8"));
			 
			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
			  hexString.append(String.format("%02X", 0xFF & b));
			}
			senha = hexString.toString();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return senha;
		
	}
	
	public static HttpSession getSession(){
		
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

	public static HttpServletRequest getRequest(){
		
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
	}
	
	public static String cpfComMascara(String cpf){
		if(cpf.contains(".")){
			cpf = Util.cpfSemMascara(cpf);
		}
		Long cpfConvertido = Long.valueOf(cpf.trim());
		MaskFormatter mascara;
		String mascarado = "";
		Formatter f = new Formatter().format("%011d", cpfConvertido);
		String s = f.toString();
		try {
			mascara = new MaskFormatter("###.###.###-##");
			mascara.setValueContainsLiteralCharacters(false);  
			mascarado = cpfConvertido!=null ? mascara.valueToString(s):"";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return mascarado;
	}
	
	public static String cpfSemMascara(String cpf){
		
		String cfpSemMascara = cpf.replaceAll("[.-]", "");
		return cfpSemMascara;
	}
	
	public static String tipoComunidade(String nomeComununidade){
		String caractFormatada="";
		if(nomeComununidade.contains("tradicional") || nomeComununidade.contains("Tradicional")){
			caractFormatada = "T";
		}else if(nomeComununidade.contains("Indigena") || nomeComununidade.contains("indigena") || nomeComununidade.contains("ind")){
			caractFormatada = "I";
		}
		
		return caractFormatada;
		
	}
}
